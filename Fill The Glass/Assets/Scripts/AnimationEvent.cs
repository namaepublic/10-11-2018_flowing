﻿using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    [SerializeField] private DropletsSpawner _dropletsSpawner;

    private void OnValidate()
    {
        _dropletsSpawner = FindObjectOfType<DropletsSpawner>();
    }

    public void ResetDroplets()
    {
        _dropletsSpawner.PositionDroplets();
    }
}