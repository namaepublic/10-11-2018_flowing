﻿using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private GameObject _playButton;
    [SerializeField] private GameObject _tipsText;
    [SerializeField] private Animator _gatesAnimator;
    [SerializeField] private LevelController[] _levelControllers;
    [SerializeField] private GameController _gameController;
    [SerializeField] private GlassController _glassController;

    private int _level;

    private void OnValidate()
    {
        _levelControllers = GetComponentsInChildren<LevelController>(true);
        _gameController = FindObjectOfType<GameController>();
        _glassController = FindObjectOfType<GlassController>();
    }

    public void OnClickPlayButton()
    {
        _playButton.SetActive(false);
        _gatesAnimator.SetTrigger("OpenGate");
        _glassController.Init();
    }

    public void Init()
    {
        _level = 0;
        foreach (var levelController in _levelControllers)
            levelController.gameObject.SetActive(false);
        ResetLevels();
        _level = 0;
    }

    private void ResetLevels()
    {
        _gatesAnimator.SetTrigger("CloseGate");
        _glassController.ResetGlass(_levelControllers[_level].RequiredDropletsQuantity);
        _playButton.SetActive(true);
        _tipsText.SetActive(_levelControllers[_level].ContainsDraggableObjects);
        _level++;
    }

    public void ReplayLevel()
    {
        _level--;
        ResetLevels();
    }

    public void NextLevel()
    {
        if (_level >= _levelControllers.Length)
        {
            _gameController.GameOver();
            return;
            ;
        }

        for (var i = 0; i < _levelControllers.Length; i++)
            _levelControllers[i].gameObject.SetActive(i == _level);

        ResetLevels();
    }
}