﻿using UnityEngine;

public class DeadZoneController : MonoBehaviour 
{
	[SerializeField] [HideInInspector] private int _dropletLayer;
	
	private void OnValidate()
	{
		_dropletLayer = LayerMask.NameToLayer("Droplet");
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.layer == _dropletLayer)
			other.gameObject.SetActive(false);
	}
}
