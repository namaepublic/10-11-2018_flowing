﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GlassController : MonoBehaviour
{
    [SerializeField] [HideInInspector] private int _dropletLayer;
    [SerializeField] private GameController _gameController;
    [SerializeField] private Image _fillProgressBar;
    [SerializeField] private TextMeshProUGUI _progressText;

    private int _requiredDropletsQuantity;
    private int _currentDropletsQuantity;
    private bool _isInit;

    public int CurrentDropletsQuantity
    {
        get { return _currentDropletsQuantity; }
        set
        {
            _currentDropletsQuantity = value;
            if (_currentDropletsQuantity >= _requiredDropletsQuantity)
                _gameController.NextLevel();

            _fillProgressBar.fillAmount = (float) _currentDropletsQuantity / _requiredDropletsQuantity;
            _progressText.text = _currentDropletsQuantity + "/" + _requiredDropletsQuantity;
        }
    }

    private void OnValidate()
    {
        _dropletLayer = LayerMask.NameToLayer("Droplet");
        _gameController = FindObjectOfType<GameController>();
    }

    public void Init()
    {
        CurrentDropletsQuantity = 0;
        _isInit = true;
    }

    public void ResetGlass(int requiredDropletQuantity)
    {
        _requiredDropletsQuantity = requiredDropletQuantity;
        _isInit = false;
        CurrentDropletsQuantity = 0;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isInit && other.gameObject.layer == _dropletLayer) CurrentDropletsQuantity++;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (_isInit && other.gameObject.layer == _dropletLayer) CurrentDropletsQuantity--;
    }
}