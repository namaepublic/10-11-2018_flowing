﻿using UnityEngine;

public class DraggableObstacle : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Color32 _defaultColor, _onDragColor;
    [SerializeField] private LayerMask _toDetect;

    private void OnValidate()
    {
        _camera = FindObjectOfType<Camera>();
        Init();
    }

    public void Init()
    {
        _spriteRenderer.color = _defaultColor;
    }

    public void OnMouseDrag()
    {
        var position = _camera.ScreenToWorldPoint(Input.mousePosition);
        position.z = 0;
        transform.position = position;
        _spriteRenderer.color = _onDragColor;
    }

    private void OnMouseUp()
    {
        _spriteRenderer.color = _defaultColor;
    }
}