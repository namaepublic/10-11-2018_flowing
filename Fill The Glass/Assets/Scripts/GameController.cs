﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private LevelsController _levelsController;

    private void OnValidate()
    {
        _levelsController = FindObjectOfType<LevelsController>();
    }

    private void Awake()
    {
        _levelsController.Init();
        NextLevel();
    }

    public void NextLevel()
    {
        _levelsController.NextLevel();
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
