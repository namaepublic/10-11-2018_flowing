﻿using UnityEditor;
using UnityEngine;

public class DropletsSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _dropletPrefab;
    [SerializeField] private int _dropletsNumber;
    [SerializeField] private Vector2 _minSpawnPosition, _maxSpawnPosition;
    [SerializeField] private float _spaceBetweenDroplets;
    [SerializeField] private Transform _dropletParent;
    [SerializeField] [HideInInspector] private Vector2 _topLeftPosition, _topRightPosition, _bottomLeftPosition, _bottomRightPosition;
    [SerializeField] private GameObject[] _droplets;

    public void PositionDroplets()
    {
        var position = _bottomLeftPosition;
        foreach (var droplet in _droplets)
        {
#if UNITY_EDITOR
            _maxSpawnPosition.y = position.y;
#endif
            droplet.transform.position = position;
            if (position.x < _maxSpawnPosition.x)
                position.x += _spaceBetweenDroplets;
            else
            {
                position.x = _minSpawnPosition.x;
                position.y += _spaceBetweenDroplets;
            }
            droplet.SetActive(true);
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        _topLeftPosition = new Vector2(_minSpawnPosition.x, _maxSpawnPosition.y);
        _topRightPosition = _maxSpawnPosition;
        _bottomLeftPosition = _minSpawnPosition;
        _bottomRightPosition = new Vector2(_maxSpawnPosition.x, _minSpawnPosition.y);
        Gizmos.DrawLine(_topLeftPosition, _topRightPosition);
        Gizmos.DrawLine(_topRightPosition, _bottomRightPosition);
        Gizmos.DrawLine(_bottomRightPosition, _bottomLeftPosition);
        Gizmos.DrawLine(_bottomLeftPosition, _topLeftPosition);
    }

    private void CreateDroplets()
    {
        DestroyAllChildren(_dropletParent);
        _droplets = new GameObject[_dropletsNumber];
        for (var i = 0; i < _dropletsNumber; i++)
            _droplets[i] = Instantiate(_dropletPrefab, _dropletParent);
        PositionDroplets();
    }

    public static void DestroyAllChildren(Transform transform)
    {
        for (var i = transform.childCount; i > 0; i--)
            DestroyImmediate(transform.GetChild(0).gameObject);
    }

    [CustomEditor(typeof(DropletsSpawner))]
    private class GatesControllerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var gatesController = (DropletsSpawner) target;

            if (GUILayout.Button("Create Droplets"))
                gatesController.CreateDroplets();
        }
    }

#endif
}