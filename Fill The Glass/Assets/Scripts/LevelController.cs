﻿using UnityEngine;

public class LevelController : MonoBehaviour
{
	[SerializeField] private int _requiredDropletsQuantity;
	[SerializeField] private bool _containsDraggableObjects;

	public int RequiredDropletsQuantity
	{
		get { return _requiredDropletsQuantity; }
	}

	public bool ContainsDraggableObjects
	{
		get { return _containsDraggableObjects; }
	}
}
